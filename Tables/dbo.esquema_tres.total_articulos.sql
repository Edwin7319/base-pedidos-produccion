CREATE TABLE [dbo].[esquema_tres.total_articulos]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[numero_producto] [int] NOT NULL,
[total_pedidos] [int] NOT NULL CONSTRAINT [DF_Doc_Exz_Column_B] DEFAULT ((0))
) ON [TERCIARIO]
GO
ALTER TABLE [dbo].[esquema_tres.total_articulos] ADD CONSTRAINT [PK__esquema___3213E83F25262CF1] PRIMARY KEY CLUSTERED  ([id]) ON [TERCIARIO]
GO
