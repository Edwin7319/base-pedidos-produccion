CREATE TABLE [dbo].[movimientos.cabezacuerpoP]
(
[numero_pedido] [int] NOT NULL IDENTITY(1, 1),
[fecha] [date] NULL,
[observaciones] [varchar] (255) COLLATE Modern_Spanish_CI_AI NULL,
[numero_cliente] [int] NULL,
[numero_detalle_pedido] [int] NULL,
[cantidad_total] [decimal] (10, 2) NULL,
[precio_unitario] [decimal] (10, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movimientos.cabezacuerpoP] ADD CONSTRAINT [PK__movimien__7F98F76C4EA876C5] PRIMARY KEY CLUSTERED  ([numero_pedido]) ON [PRIMARY]
GO
