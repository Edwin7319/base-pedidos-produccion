CREATE TABLE [dbo].[catalogo.fabrica]
(
[numero_fabrica] [int] NOT NULL,
[contacto] [varchar] (255) COLLATE Modern_Spanish_CI_AI NULL,
[nombre] [varchar] (255) COLLATE Modern_Spanish_CI_AI NOT NULL,
[descripcion] [varchar] (255) COLLATE Modern_Spanish_CI_AI NULL,
[telefono] [varchar] (12) COLLATE Modern_Spanish_CI_AI NULL
) ON [SECONDARY]
GO
ALTER TABLE [dbo].[catalogo.fabrica] ADD CONSTRAINT [PK__catalogo__4F60943A5E32794A] PRIMARY KEY CLUSTERED  ([numero_fabrica]) ON [SECONDARY]
GO
