CREATE TABLE [dbo].[catalogo.producto]
(
[numero_producto] [int] NOT NULL IDENTITY(1, 1),
[descripcion] [varchar] (255) COLLATE Modern_Spanish_CI_AI NULL,
[cantidad_stock] [int] NOT NULL,
[precio] [decimal] (10, 2) NULL
) ON [SECONDARY]
GO
ALTER TABLE [dbo].[catalogo.producto] ADD CONSTRAINT [PK__catalogo__A7DAA3E59ADA7B49] PRIMARY KEY CLUSTERED  ([numero_producto]) ON [SECONDARY]
GO
