CREATE TABLE [dbo].[catalogo.cliente]
(
[numero_cliente] [int] NOT NULL IDENTITY(1, 1),
[limite_credito] [decimal] (10, 2) NOT NULL CONSTRAINT [DF__catalogo.__limit__36B12243] DEFAULT ((1)),
[descuento] [decimal] (10, 2) NULL,
[credito] [decimal] (10, 2) NOT NULL,
[direccion] [varchar] (255) COLLATE Modern_Spanish_CI_AI NULL,
[codigo_garante] [int] NULL
) ON [SECONDARY]
GO
ALTER TABLE [dbo].[catalogo.cliente] ADD CONSTRAINT [PK__catalogo__DA28746243B27EAE] PRIMARY KEY CLUSTERED  ([numero_cliente]) ON [SECONDARY]
GO
CREATE NONCLUSTERED INDEX [IX_Relationship25] ON [dbo].[catalogo.cliente] ([codigo_garante]) ON [SECONDARY]
GO
