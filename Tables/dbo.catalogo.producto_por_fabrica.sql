CREATE TABLE [dbo].[catalogo.producto_por_fabrica]
(
[fecha] [date] NOT NULL,
[productos_entregados] [int] NOT NULL,
[numero_fabrica] [int] NOT NULL,
[numero_producto] [int] NOT NULL
) ON [SECONDARY]
GO
ALTER TABLE [dbo].[catalogo.producto_por_fabrica] ADD CONSTRAINT [PK_producto_por_fabrica] PRIMARY KEY CLUSTERED  ([numero_fabrica], [numero_producto]) ON [SECONDARY]
GO
ALTER TABLE [dbo].[catalogo.producto_por_fabrica] ADD CONSTRAINT [Relationship18] FOREIGN KEY ([numero_fabrica]) REFERENCES [dbo].[catalogo.fabrica] ([numero_fabrica])
GO
ALTER TABLE [dbo].[catalogo.producto_por_fabrica] ADD CONSTRAINT [Relationship24] FOREIGN KEY ([numero_producto]) REFERENCES [dbo].[catalogo.producto] ([numero_producto])
GO
