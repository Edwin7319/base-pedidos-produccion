CREATE TABLE [dbo].[movimientos.detalle_pedido]
(
[numero_detalle_pedido] [int] NOT NULL IDENTITY(1, 1),
[precio_unitario] [decimal] (10, 2) NULL,
[numero_pedido] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movimientos.detalle_pedido] ADD CONSTRAINT [PK__movimien__A5437DA238B926ED] PRIMARY KEY CLUSTERED  ([numero_detalle_pedido]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Relationship23] ON [dbo].[movimientos.detalle_pedido] ([numero_pedido]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movimientos.detalle_pedido] ADD CONSTRAINT [Relationship23] FOREIGN KEY ([numero_pedido]) REFERENCES [dbo].[movimientos.cabecera_pedido] ([numero_pedido])
GO
