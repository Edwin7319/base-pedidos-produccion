CREATE TABLE [dbo].[movimientos.cabecera_pedido]
(
[numero_pedido] [int] NOT NULL IDENTITY(1, 1),
[fecha] [date] NOT NULL,
[observaciones] [varchar] (255) COLLATE Modern_Spanish_CI_AI NULL,
[numero_cliente] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movimientos.cabecera_pedido] ADD CONSTRAINT [PK__movimien__7F98F76CF9D969A3] PRIMARY KEY CLUSTERED  ([numero_pedido]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Relationship22] ON [dbo].[movimientos.cabecera_pedido] ([numero_cliente]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movimientos.cabecera_pedido] ADD CONSTRAINT [Relationship22] FOREIGN KEY ([numero_cliente]) REFERENCES [dbo].[catalogo.cliente] ([numero_cliente])
GO
