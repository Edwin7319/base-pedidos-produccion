CREATE TABLE [dbo].[movimientos.detalle_pedido_por_producto]
(
[numero_detalle_pedido] [int] NOT NULL,
[numero_producto] [int] NOT NULL,
[cantidad_total] [int] NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trigger_actualizar_total_articulos]
ON [dbo].[movimientos.detalle_pedido_por_producto]
    AFTER INSERT, UPDATE, DELETE
AS
BEGIN
  DECLARE @idProducto INT
  DECLARE @idDetallePedido INT
  DECLARE @totalProductos INT
  SET @idProducto = (SELECT I.numero_producto FROM Inserted AS I)
  SET @idDetallePedido = (SELECT I.numero_detalle_pedido FROM Inserted AS I)
  SET @totalProductos = (SELECT I.cantidad_total FROM Inserted AS I)
   
    IF EXISTS ( SELECT 0 FROM Deleted )
        BEGIN
            IF EXISTS ( SELECT 0 FROM Inserted )
               BEGIN
                    update [esquema_tres.total_articulos]
                            SET
                            total_pedidos = total_pedidos + @totalProductos
							FROM  deleted as D
							WHERE
                            D.numero_producto = @idProducto
				
                END
            ELSE
                BEGIN
                    delete from [esquema_tres.total_articulos]
                            FROM    deleted as D
					        where  D.numero_producto = @idProducto
                END  
        END
    ELSE
        BEGIN
            INSERT  INTO  [esquema_tres.total_articulos]
                           ( 
						   numero_producto,
						   total_pedidos
                            )
                            SELECT  
							@idProducto,
							@totalProductos
                            FROM inserted as I
	
        END
 END
GO
ALTER TABLE [dbo].[movimientos.detalle_pedido_por_producto] ADD CONSTRAINT [PK_detalle_pedido_por_producto] PRIMARY KEY CLUSTERED  ([numero_detalle_pedido], [numero_producto]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[movimientos.detalle_pedido_por_producto] ADD CONSTRAINT [Relationship20] FOREIGN KEY ([numero_detalle_pedido]) REFERENCES [dbo].[movimientos.detalle_pedido] ([numero_detalle_pedido])
GO
ALTER TABLE [dbo].[movimientos.detalle_pedido_por_producto] ADD CONSTRAINT [Relationship21] FOREIGN KEY ([numero_producto]) REFERENCES [dbo].[catalogo.producto] ([numero_producto])
GO
